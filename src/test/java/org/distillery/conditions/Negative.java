package org.distillery.conditions;

import com.google.common.base.CharMatcher;

import org.testng.Assert;

public class Negative {
	private final String VALID_SYMBOLS = "ABCDF";
	private String data;
	
	public Negative() {}
	
	/* ------------------------<Публичные методы>------------------------ */
	
	public void setName(String data)
	{
		this.data = data;
	}
	
	/**
	 * Метод для проверки всех негативных условий: null, empty, invalid chars, nums.
	 */
	public void checkAllNegativeConditions() {
		Assert.assertFalse(isNullString(), "Failed! String data is null.");
		System.out.println("ok...data is not null");
		Assert.assertFalse(isEmptyString(), "Failed! String data is empty.");
		System.out.println("ok...data is not empty");
		Assert.assertFalse(isInvalidSymbolExist(), "Failed! String data contains invalid characters. [guava]");
		System.out.println("ok...data is not contains invalid characters. [guava]");
		Assert.assertTrue(isInvalidSymbolExistMineSolution(), "Failed! String data contains invalid characters. [mine]");
		System.out.println("ok...data is not contains invalid characters. [mine]");
		Assert.assertFalse(isMaxLengthString(), "Failed! String data contains nums of characters more than 2^31 - 1.");
		System.out.println("ok...data is not contains nums of characters more than 2^31 - 1.");
	}
	
	public boolean isNullString()
    {
        return null == this.data ? true : false;
    }
	
	public boolean isEmptyString()
    {
        return this.data.isEmpty() ? true : false;
    }
	
	public boolean isMaxLengthString()
    {
        return this.data.length() > Integer.MAX_VALUE ? true : false;
    }
	
	public boolean isInvalidSymbolExist()
    {
		return !CharMatcher.anyOf(VALID_SYMBOLS).matchesAnyOf(this.data) ? true : false;
    }
	
	public boolean isInvalidSymbolExistMineSolution()
    {
		String data = this.data;
		
		for (int i=0; i<data.length(); i++) { if (data.charAt(i) > 'F' || data.charAt(i) == 'E') return false; }
		
		return true;
    }
}
