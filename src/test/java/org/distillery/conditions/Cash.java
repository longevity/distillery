package org.distillery.conditions;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import org.distillery.shop.App;
import org.testng.Assert;

public class Cash {
	public static void checkAllCashValuesValid()
    {
		Map<String, Double> prices = new HashMap<>();
		
		prices.put("ABCDABA", 793.60);
		prices.put("CCCCCCC", 6.00);
		prices.put("ABCD", 632.80);
		
		for(String price : new String[] {"ABCDABA", "CCCCCCC", "ABCD"}) 
		{
			Assert.assertTrue(isEqualInRangeToValue(App.getPrice(price), prices.get(price), 0.05, 0.0, 2), 
					"Failed! price " + price + " is not valid. Incorrect cash value is " + App.getPrice(price));
			System.out.println("ok...price is correct.");
		}
    }
	
	/** 
	 * Проверка, что текущее значение индикатора отличается от значения, переданного в аргументе, 
	 * на заданную величину в указанном диапазоне
	 * @param fValue, sValue Величины, для сравнения их значений.
	 * @param range Диапазон сравнения
	 * @param delta Величина, на которую должны отличаться величины. 
	 *  Если задана  delta, то сравниваемое значение предварительно увеличивается на величину delta.
	 * @param precise Требуемая точность округления при сравнении
	 * @return Величины равны (true) / не равны (false)
	 * */
	public static boolean isEqualInRangeToValue(double fValue, double sValue, double range, double delta, int precise) {
		Assert.assertTrue(range >= 0, "Range value " + range + " is negative");
		return roundResult( Math.abs(fValue - (sValue + delta)), precise ) <= range ;				
	}
	
	/**
	 * Метод округления
	 * @param value значение типа double, которое требуется округлить.
	 * @param precise требуемая точность округления.
	 * @return значение типа double, округленное до указанной точности. 
	 */
	public static double roundResult (double value, int precise)
	{
		return new BigDecimal(""+value).setScale(precise, RoundingMode.HALF_UP).doubleValue();
	}
}
