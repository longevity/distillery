package org.distillery.negative;

import org.distillery.conditions.Negative;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class InvalidSymbolsTest {
	private Negative negativeConditions;
	
	@BeforeTest
	public void init() {
		negativeConditions = new Negative();
		negativeConditions.setName("CCCCCCC");
	}
	
	@AfterTest
    public void after() { negativeConditions = null; }
	
	@Test
    public void testNegativeConditions() {
    	negativeConditions.checkAllNegativeConditions();
    	System.out.println("This is checkAllNegativeConditions");
    }
}
